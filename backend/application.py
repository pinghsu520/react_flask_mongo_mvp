from flask import Flask
from flask_cors import CORS
from datetime import timedelta
from flask_jwt_extended import JWTManager
from bson.json_util import dumps
from dotenv import load_dotenv
from src.library.routes.testRoute import testWork
from src.library.routes.adminRoute import admin
from src.library.db import testCollection  # Import testCollection from db.py

import os

load_dotenv()

application = Flask(__name__)
application.config['JWT_SECRET_KEY'] = os.getenv('JWT_SECRET_KEY')
application.config['JWT_ACCESS_TOKEN_EXPIRES'] = False  
jwt = JWTManager(application)
application.config['CORS_HEADERS'] = 'Content-Type'
CORS(application, resources={
     r"/*": {"origins": "*", "allow_headers": "*", "expose_headers": "*"}})

@application.route("/")
def healthcheck():
    return {'napoleon': 'this is a cool test'}

@application.route("/testcollection")
def add_to_test_collection():
    testCollection.insert_one({'name': 'napoleon'})
    return {'napoleon': 'this is a cool test'}

@application.route("/get_test_collection")
def get_test_collection():
    test_docs = testCollection.find()  
    return dumps(list(test_docs))

# if blah is found then return it 
@application.route("/find_blah")
def find_blah():
    test_docs = testCollection.find({'testString': 'blah'})
    return dumps(list(test_docs))

@application.route("/deleteAll")
def delete_test_collection():
    testCollection.delete_many({})
    return {'message': 'delete_successful'}

application.register_blueprint(testWork, url_prefix="/testWork")
application.register_blueprint(admin, url_prefix="/admin")

if __name__ == '__main__':
    application.run(port=8888, debug=True, host='0.0.0.0')
