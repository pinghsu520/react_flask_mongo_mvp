import os
from pymongo import MongoClient

# Grab MongoClient from .env file 
client = MongoClient(os.getenv('MONGO_URI'))
# Creating a database in MongoDB
db = client.test_database
# Creating a collection in this db
testCollection = db.testCollection
