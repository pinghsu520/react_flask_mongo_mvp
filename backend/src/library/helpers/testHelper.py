from src.library.db import testCollection  # Import testCollection from db.py

def insertFormFromFrontEndHelper(testID, testString, testParagraph):
    testDict = {
        "testID": testID,
        "testString": testString,
        "testParagraph": testParagraph
    }
    testCollection.insert_one(testDict)
    return {'Success': True}
