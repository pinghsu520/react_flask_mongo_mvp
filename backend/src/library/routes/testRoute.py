from flask import Blueprint, request, jsonify
from flask_cors import cross_origin
# from flask_cors import cross_origin
from werkzeug.utils import secure_filename
import time
import os

import math
from src.library.helpers.testHelper import insertFormFromFrontEndHelper

testWork= Blueprint('c', __name__)


@testWork.route('/', methods=['GET'])
def agentTest():
    return {'agnet': 'this is a agent test'}

@testWork.route('/addTest', methods=['POST'])
def insertFormFromFrontend():
    body = request.json
    testID=body.get('testID', '')
    testString = body.get('testString', '')
    testParagraph=body.get('testParagraph', '')
    insertFormFromFrontEndHelper(testID, testString, testParagraph)
    return {'message': 'Inputted form into db succesfully!'} 